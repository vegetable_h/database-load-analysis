c_LANL = importdata('~/expr/database-load-analysis/cPer/cPer_L.out');
c_SDSC = importdata('~/expr/database-load-analysis/cPer/cPer_S.out');
c_PIK = importdata('~/expr/database-load-analysis/cPer/cPer_P.out');

LANL = importdata('~/expr/database-load-analysis/tPer/tPer_L.dat');
SDSC = importdata('~/expr/database-load-analysis/tPer/tPer_S.dat');
PIK = importdata('~/expr/database-load-analysis/tPer/tPer_P.dat');
data = zeros(10,7);
x = 10:-1:1;
for i = 1:10 data(i,1) = LANL(i); end
for i = 1:10 data(i,2) = PIK(i); end
for i = 1:10 data(i,3) = SDSC(i); end

for i = 1:10 data(i,5) = c_LANL(i); end
for i = 1:10 data(i,6) = c_PIK(i); end
for i = 1:10 data(i,7) = c_SDSC(i); end
figure
bar(x,data);
set(gca,'XTickLabel',{'10:1','10:2','10:3','10:4','10:5','10:6','10:7', '10:8', '10:9', '10:10'});
legend('LANL','PIK', 'SDSC','LANL_combine', 'PIK_combine', 'SDSC_combine');
ylabel('Performance');
xlabel('DG capacity:FC capacity');