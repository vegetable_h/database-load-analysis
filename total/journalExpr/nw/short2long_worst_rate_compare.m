
x = [2.26085, 1.52151, 2.21453,   4.76, 2.64286;
     2.26085, 1.52151, 2.09165,   1,    2.56522;
     2.26085, 1.35903, 2.13652,   1,    2.64286;
     1.1393,  3.30584, 2.12979,   1,    2.64286;];

 figure
bar(x, 'grouped');
set(gca,'XTickLabel',{'no error','sigma = 0.5','sigma = 1','sigma = 1.5'})
legend('HPC2N', 'LANL', 'LANL-O2K', 'PIK', 'SDSC-BLUE');
xlabel('error rate');
ylabel('worst performance');


