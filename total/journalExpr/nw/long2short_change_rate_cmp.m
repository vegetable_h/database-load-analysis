
x = [23219, 98636,  40031,  65076;
     23105, 96882,  39760,  63302;
     22785, 92890,  39358,  61986;
     22749, 88034,  38936,  60034;];

 figure
bar(x, 'grouped');
set(gca,'XTickLabel',{'no error','sigma = 0.5','sigma = 1','sigma = 1.5'})
legend('HPC2N', 'LANL', 'LANL-O2K', 'SDSC-BLUE');
xlabel('error rate');
ylabel('DG change rate');


