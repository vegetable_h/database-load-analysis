
x = [1.02156, 1.00653, 1.00814,   1.93837, 1.04038;
     1.29375, 1.27965, 1.28122,   1.48099, 1.32204;
     1.8815,  1.85836, 1.84293,   2.01079, 1.92942;
     3.35359, 3.33371, 3.23537,   1.64394, 3.31406;];

 figure
bar(x, 'grouped');
set(gca,'XTickLabel',{'no error','sigma = 0.5','sigma = 1','sigma = 1.5'})
legend('HPC2N', 'LANL', 'LANL-O2K', 'PIK', 'SDSC-BLUE');
xlabel('error rate');
ylabel('worst performance');


