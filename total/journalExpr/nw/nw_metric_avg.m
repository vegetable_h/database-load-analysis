
x = [1.02156, 1.00653, 1.00482, 1.04038, 2.16439;
     1.02328, 1.00749, 1.0048,  1.04127, 6.87049;
     1.02457, 1.01087, 1.00924, 1.04604, 2.16439;
     1.03746, 1.01775, 1.01659, 1.05896, 2.16439;];

 figure
bar(x, 'grouped');
set(gca,'XTickLabel',{'no error','sigma = 0.5','sigma = 1','sigma = 1.5'})
legend('HPC2N', 'LANL', 'PIK', 'SDSC-BLUE', 'SDSC-DS');
xlabel('error rate');
ylabel('average case performance');


