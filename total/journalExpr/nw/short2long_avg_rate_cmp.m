
x = [1.02156,  1.00653,  1.00814,   1.93837, 1.04038;
     0.854868, 0.861683, 0.852195,  0.855106, 0.886456;
     0.823308, 0.760601, 0.77145,   0.674629, 0.786913;
     0.712833, 0.714963, 0.705515,  0.641807, 0.73639;];

 figure
bar(x, 'grouped');
set(gca,'XTickLabel',{'no error','sigma = 0.5','sigma = 1','sigma = 1.5'})
legend('HPC2N', 'LANL', 'LANL-O2K', 'PIK', 'SDSC-BLUE');
xlabel('error rate');
ylabel('worst performance');


