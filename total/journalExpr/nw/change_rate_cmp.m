
x = [23219, 98636,  29634,  65076, 18302;
     23537, 99382,  29546,  67300, 25660;
     23281, 98562,  30200,  73406, 18302;
     23105, 100268, 30552,  79020, 18302;];

 figure
bar(x, 'grouped');
set(gca,'XTickLabel',{'no error','sigma = 0.5','sigma = 1','sigma = 1.5'})
legend('HPC2N', 'LANL', 'LANL-O2K', 'SDSC-BLUE', 'SDSC-DS');
xlabel('error rate');
ylabel('DG change rate');


